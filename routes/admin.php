<?php

use App\Http\Livewire\Admin\Invites;
use App\Http\Livewire\Admin\Leds;
use App\Http\Livewire\Admin\Rooms;
use App\Http\Livewire\Admin\Users;
use App\Http\Livewire\Admin\AuthTokens;
use Illuminate\Support\Facades\Route;

Route::get('users', Users::class)->name('users');
Route::get('invites', Invites::class)->name('invites');
Route::get('rooms', Rooms::class)->name('rooms');
Route::get('leds', Leds::class)->name('leds');
Route::get('tokens', AuthTokens::class)->name('tokens');
