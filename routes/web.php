<?php

use App\Http\Controllers\LedController;
use App\Http\Livewire\Dashboard;
use App\Http\Livewire\Logs;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::redirect('/', '/login');

Route::get('/health', function() {
    return 'OK';
});

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', Dashboard::class)->name('dashboard');
    Route::get('/logs', Logs::class)->name('logs');
});

Route::middleware(['localNetwork'])->group(function () {
    Route::post('/leds', [LedController::class, 'store']);
    Route::delete('/leds', [LedController::class, 'destroy']);
});

require_once __DIR__ . '/auth.php';
