<?php

namespace Database\Seeders;

use App\Models\Led;
use Illuminate\Database\Seeder;

class LedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Led::factory()->count(5)->create();
    }
}
