<?php

namespace Database\Factories;

use App\Models\Led;
use Illuminate\Database\Eloquent\Factories\Factory;

class LedFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Led::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'ip' => $this->faker->localIpv4(),
            'callback_url' => '/' . $this->faker->slug(2, false),
            'ssl' => $this->faker->boolean(),
        ];
    }
}
