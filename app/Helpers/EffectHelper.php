<?php

namespace App\Helpers;

class EffectHelper
{
    public const EFFECTS = [
        'breathing',
        'campfire',
        'comet',
        'progress-bar',
        'rainbow-sine-static',
        'rainbow-sine-wave',
        'rainbow-static',
        'rainbow-wave',
        'static-color',
    ];
}
