<?php

namespace App\Actions;

use App\Models\Led;
use App\Models\AuthToken;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;

class SendToLed
{
    public static function send($led, $properties)
    {
        $led = Led::findOrFail($led);

        $protocol = $led->ssl ? 'https' : 'http';
        $host = $led->ip;
        $pathname = $led->callback_url;

        $token = AuthToken::where('led_ip', $host)->firstOrFail();
        $auth_token = $token->auth_token;

        try {
            Http::withHeaders([
                'Authorization' => "Bearer {$auth_token}"
                ])->post("{$protocol}://{$host}{$pathname}", [
                'infos' => $properties,
            ]);
        } catch (ConnectionException $e) {
            return $e->getMessage();
        }

        return null;
    }
}
