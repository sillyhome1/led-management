<?php

namespace App\Actions;

use App\Models\Room;
use App\Models\AuthToken;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;

class SendToRoom
{
    public static function send($room, $properties)
    {
        $errors = collect();
        $room = Room::findOrFail($room);
        $leds = $room->leds;

        foreach ($leds as $led) {
            $protocol = $led->ssl ? 'https' : 'http';
            $host = $led->ip;
            $pathname = $led->callback_url;

            $token = AuthToken::where('led_ip', $host)->firstOrFail();
            $auth_token = $token->auth_token;

            try {
                Http::withHeaders([
                    'Authorization' => "Bearer {$auth_token}"
                    ])->post("{$protocol}://{$host}{$pathname}", [
                    'infos' => $properties,
                ]);
            } catch (ConnectionException $e) {
                $errors->push($e->getMessage());
            }
        }

        return $errors->toArray();
    }
}
