<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Led extends Model
{
    use HasFactory;

    protected $fillable = ['ip', 'callback_url', 'ssl'];

    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }
}
