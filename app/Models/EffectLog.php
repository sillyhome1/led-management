<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EffectLog extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'effect', 'send_to_type', 'send_to_entity', 'properties'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
