<?php

namespace App\Http\Livewire;

use App\Models\Led;
use App\Models\AuthToken;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Livewire\Component;

class Status extends Component
{
    public bool $show = false;
    public Collection $leds;

    public function mount(): void
    {
        $this->leds = Led::get();
    }

    protected $listeners = [
        'show-status' => 'load'
    ];

    public function load()
    {
        $this->leds->each(function ($led) {
            $led->status = $this->getStatus($led);
        })->filter(fn ($led) => (bool) $led->status);

        $this->show = true;
    }

    private function getStatus(Led $led): ?\stdClass
    {
        try {
            $host = $led->ip;

            $token = AuthToken::where('led_ip', $host)->firstOrFail();
            $auth_token = $token->auth_token;

            $response = Http::withHeaders(['Authorization' => "Bearer {$auth_token}"])
                ->timeout(3)
                ->get($this->getUri($led));
        } catch (\Exception $e) {
            return null;
        }

        if ($response->status() !== 200) {
            return null;
        }

        return (object) [
            'led' => $led->callback_url,
            'json' => json_encode($response->json(), JSON_PRETTY_PRINT)
        ];
    }

    private function getUri(Led $led): string
    {
        $protocol = $led->ssl ? 'https' : 'http';
        $host = $led->ip;
        $pathname = $led->callback_url;

        return "{$protocol}://{$host}{$pathname}";
    }

    public function render()
    {
        return view('livewire.status');
    }
}
