<?php

namespace App\Http\Livewire;

use App\Actions\SendToLed;
use App\Actions\SendToRoom;
use App\Helpers\EffectHelper;
use App\Models\EffectLog;
use App\Models\Led;
use App\Models\Room;
use Livewire\Component;

class Dashboard extends Component
{
    public $effect;
    public $effect_properties;
    public $sendToType;
    public $sendToEntity;
    public $rooms;
    public $leds;
    public $types = [
        'led' => 'LED',
        'room' => 'Room',
    ];

    protected $listeners = [
        'receive-effect-properties' => 'send',
    ];

    public function updatedSendToType($type)
    {
        $this->sendToEntity = (string) ($this->{$type . 's'}->pluck('id')->toArray()[0] ?? 1);
    }

    public function mount()
    {
        $this->rooms = Room::get();
        $this->leds = Led::get();

        // Set Default if no log found
        $this->effect = EffectHelper::EFFECTS[0];
        $this->sendToType = 'led';
        $this->sendToEntity = $this->leds->first()->id ?? 1;

        // get latest effect log entry
        $log = EffectLog::latest()->first();

        // Override defaults with data from latest log entry
        if ($log) {
            $this->effect = $log->effect;
            $this->sendToType = $log->send_to_type;
            $this->sendToEntity = $log->send_to_entity;

            if (!$this->{$this->sendToType . 's'}->find($this->sendToEntity)) {
                $this->sendToEntity = $this->{$this->sendToType . 's'}->first()?->id ?? 1;
            }
        }
    }

    public function prepareSend()
    {
        $this->emit('request-effect-properties');
    }

    public function turnOff()
    {
        if ($this->sendToType == 'led') {
            $error = SendToLed::send((int) $this->sendToEntity, [
                'effect_group' => 'static-color',
                'effect_value' => '#000000',
            ]);

            if ($error) {
                $this->alert('error', $error, 1000 * 10);
            }
        }

        if ($this->sendToType == 'room') {
            $errors = SendToRoom::send($this->sendToEntity, [
                'effect_group' => 'static-color',
                'effect_value' => '#000000',
            ]);

            foreach ($errors as $error) {
                $this->alert('error', $error, 1000 * 10);
            }
        }

        $this->alert('success', 'Lights turned off successfully.');
    }

    public function send($properties)
    {
        $this->effect_properties = $properties;

        if ($this->sendToType == 'led') {
            $error = SendToLed::send((int) $this->sendToEntity, [
                'effect_group' => $this->effect,
            ] + $this->effect_properties);

            if ($error) {
                $this->alert('error', $error, 1000 * 10);
            }
        }

        if ($this->sendToType == 'room') {
            $errors = SendToRoom::send($this->sendToEntity, [
                'effect_group' => $this->effect,
            ] + $this->effect_properties);

            foreach ($errors as $error) {
                $this->alert('error', $error, 1000 * 10);
            }
        }

        EffectLog::create([
            'user_id' => auth()->user()->id,
            'effect' => $this->effect,
            'send_to_type' => $this->sendToType,
            'send_to_entity' => $this->sendToEntity,
            'properties' => json_encode($this->effect_properties),
        ]);

        $this->alert('success', 'Request successfully sent.');
    }

    public function render()
    {
        return view('livewire.dashboard')
            ->layout('layouts.app', ['header' => 'Dashboard']);
    }
}
