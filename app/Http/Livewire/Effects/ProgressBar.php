<?php

namespace App\Http\Livewire\Effects;

use App\Models\EffectLog;
use Livewire\Component;

class ProgressBar extends Component
{
    public array $colors = [];
    public $duration = 10;
    public $count = 0;
    public $reverse = false;

    protected $listeners = [
        'request-effect-properties' => 'emitEffectProperties',
    ];

    public function emitEffectProperties()
    {
        $this->emitUp('receive-effect-properties', [
            'effect_value' => [
                'colours' => $this->colors,
                'duration' => (float) $this->duration,
                'count' => (int) $this->count,
                'reverse' => (bool) $this->reverse,
            ],
        ]);
    }

    public function mount()
    {
        $latestLog = EffectLog::where('effect', 'progress-bar')->latest()->first();

        if ($latestLog) {
            $values = json_decode($latestLog->properties)->effect_value;

            $this->colors = array_values((array) $values->colours);
            $this->duration = (float) $values->duration;
            $this->count = (int) $values->count;
            $this->reverse = (bool) $values->reverse;
        }
    }

    public function addColor()
    {
        $this->colors[] = '#ffffff';
    }

    public function removeColor(int $offset)
    {
        unset($this->colors[$offset]);
        $this->colors = array_values($this->colors);
    }

    public function render()
    {
        return view('livewire.effects.progress-bar');
    }
}
