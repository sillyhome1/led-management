<?php

namespace App\Http\Livewire\Effects;

use App\Models\EffectLog;
use Livewire\Component;

class Campfire extends Component
{
    public int $hue = 20;
    public float $turbulence = 1;

    protected $listeners = [
        'request-effect-properties' => 'emitEffectProperties',
    ];

    public function mount()
    {
        $latestLog = EffectLog::where('effect', 'campfire')->latest()->first();

        if ($latestLog) {
            $values = json_decode($latestLog->properties)->effect_value;

            $this->hue = (int) $values->hue;
            $this->turbulence = (float) $values->turbulence;
        }
    }

    public function emitEffectProperties()
    {
        $this->emitUp('receive-effect-properties', [
            'effect_value' => [
                'hue' => (int) $this->hue,
                'turbulence' => (float) $this->turbulence,
            ],
        ]);
    }

    public function render()
    {
        return view('livewire.effects.campfire');
    }
}
