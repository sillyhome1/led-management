<?php

namespace App\Http\Livewire\Effects;

use App\Models\EffectLog;
use Livewire\Component;

class Breathing extends Component
{
    public array $colors = [];
    public $duration = 10;

    protected $listeners = [
        'request-effect-properties' => 'emitEffectProperties',
    ];

    public function emitEffectProperties()
    {
        $this->emitUp('receive-effect-properties', [
            'effect_value' => [
                'colours' => $this->colors,
                'duration' => (float) $this->duration,
            ],
        ]);
    }

    public function mount()
    {
        $latestLog = EffectLog::where('effect', 'breathing')->latest()->first();

        if ($latestLog) {
            $values = json_decode($latestLog->properties)->effect_value;

            $this->duration = (float) $values->duration;
            $this->colors = array_values((array) $values->colours);
        }
    }

    public function addColor()
    {
        $this->colors[] = '#ffffff';
    }

    public function removeColor(int $offset)
    {
        unset($this->colors[$offset]);
        $this->colors = array_values($this->colors);
    }

    public function render()
    {
        return view('livewire.effects.breathing');
    }
}
