<?php

namespace App\Http\Livewire\Effects;

use App\Models\EffectLog;
use Livewire\Component;

class Comet extends Component
{
    public int $hue = 200;
    public float $speed = 1;
    public bool $reverse = false;

    protected $listeners = [
        'request-effect-properties' => 'emitEffectProperties',
    ];

    public function mount()
    {
        $latestLog = EffectLog::where('effect', 'comet')->latest()->first();

        if ($latestLog) {
            $values = json_decode($latestLog->properties)->effect_value;

            $this->hue = (int) $values->hue;
            $this->speed = (float) $values->speed;
            $this->reverse = (bool) $values->reverse;
        }
    }

    public function emitEffectProperties()
    {
        $this->emitUp('receive-effect-properties', [
            'effect_value' => [
                'hue' => (int) $this->hue,
                'speed' => (float) $this->speed,
                'reverse' => (bool) $this->reverse,
            ],
        ]);
    }

    public function render()
    {
        return view('livewire.effects.comet');
    }
}
