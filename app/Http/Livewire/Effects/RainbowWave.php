<?php

namespace App\Http\Livewire\Effects;

use App\Models\EffectLog;
use Livewire\Component;

class RainbowWave extends Component
{
    public $duration = 10;
    public bool $reverse = false;

    protected $listeners = [
        'request-effect-properties' => 'emitEffectProperties',
    ];

    public function updatedDuration($value)
    {
        $float = (float) str_replace(',', '.', $value);

        ($value !== '0' && $float === 0.0)
            ? $this->reset('duration')
            : $this->duration = $float;
    }

    public function mount()
    {
        $latestLog = EffectLog::where('effect', 'rainbow-wave')->latest()->first();

        if ($latestLog) {
            $values = json_decode($latestLog->properties)->effect_value;

            $this->duration = (float) $values->duration;
            $this->reverse = (bool) isset($values->reverse) ? $values?->reverse : false;
        }
    }

    public function emitEffectProperties()
    {
        $this->emitUp('receive-effect-properties', [
            'effect_value' => [
                'duration' => (float) $this->duration,
                'reverse' => (bool) $this->reverse,
            ],
        ]);
    }

    public function render()
    {
        return view('livewire.effects.rainbow-wave');
    }
}
