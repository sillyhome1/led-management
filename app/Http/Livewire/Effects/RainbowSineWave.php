<?php

namespace App\Http\Livewire\Effects;

use App\Models\EffectLog;
use Livewire\Component;

class RainbowSineWave extends Component
{
    public $duration = 10;
    public bool $reverse = false;
    public $brightness = 0.5;

    protected $listeners = [
        'request-effect-properties' => 'emitEffectProperties',
    ];

    public function updatedDuration($value)
    {
        $float = (float) str_replace(',', '.', $value);

        ($value !== '0' && $float === 0.0)
            ? $this->reset('duration')
            : $this->duration = $float;
    }

    public function mount()
    {
        $latestLog = EffectLog::where('effect', 'rainbow-sine-wave')->latest()->first();

        if ($latestLog) {
            $values = json_decode($latestLog->properties)->effect_value;

            $this->duration = (float) $values->duration;
            $this->reverse = (bool) isset($values->reverse) ? $values?->reverse : false;
            $this->brightness = (float) isset($values->brightness) ? $values->brightness : 0.5;
        }
    }

    public function emitEffectProperties()
    {
        $this->emitUp('receive-effect-properties', [
            'effect_value' => [
                'duration' => (float) $this->duration,
                'reverse' => (bool) $this->reverse,
                'brightness' => (float) $this->brightness,
            ],
        ]);
    }

    public function render()
    {
        return view('livewire.effects.rainbow-sine-wave');
    }
}
