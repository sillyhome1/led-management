<?php

namespace App\Http\Livewire\Effects;

use App\Models\EffectLog;
use Livewire\Component;

class StaticColor extends Component
{
    public $color;

    protected $listeners = [
        'request-effect-properties' => 'emitEffectProperties',
    ];

    public function emitEffectProperties()
    {
        $this->emitUp('receive-effect-properties', [
            'effect_value' => $this->color,
        ]);
    }

    public function mount()
    {
        $latestLog = EffectLog::where('effect', 'static-color')->latest()->first();

        $this->color = ($latestLog)
            ? json_decode($latestLog->properties)->effect_value
            : '#ffffff';
    }

    public function render()
    {
        return view('livewire.effects.static-color');
    }
}
