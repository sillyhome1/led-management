<?php

namespace App\Http\Livewire;

use App\Models\EffectLog;
use App\Models\Led;
use App\Models\Room;
use Livewire\Component;

class Logs extends Component
{
    public $rooms;
    public $leds;

    public function mount()
    {
        $this->rooms = Room::get();
        $this->leds = Led::get();
    }

    public function getRowsProperty()
    {
        return EffectLog::query()->latest()->paginate(10);
    }

    public function render()
    {
        return view('livewire.logs', ['logs' => $this->rows])
            ->layout('layouts.app', ['header' => 'Effect Logs']);
    }
}
