<?php

namespace App\Http\Livewire\Admin;

use App\Models\User;
use Illuminate\Validation\Rules\Password;
use Livewire\Component;

class Users extends Component
{
    public User $editing;
    public User $deleting;
    public $showEditModal = false;
    public $showDeleteModal = false;
    public $newPassword;

    public function mount()
    {
        $this->editing = User::make();
    }

    public function edit(User $user)
    {
        if ($this->editing->isNot($user)) {
            $this->editing = $user;
        }

        $this->showEditModal = true;
    }

    public function update()
    {
        $this->validate([
            'editing.username' => 'required | unique:users,username,' . $this->editing->id,
            'editing.is_admin' => 'required | boolean',
        ]);

        if ($this->newPassword !== null) {
            $this->validate([
                'newPassword' => ['required', Password::defaults()],
            ]);

            $this->editing->password = bcrypt($this->newPassword);
        }

        $this->editing->save();

        $this->alert('success', 'Changes successfully saved.');
        $this->reset('showEditModal', 'newPassword');
    }

    public function getRowsProperty()
    {
        return User::query()->latest()->get();
    }

    public function render()
    {
        return view('livewire.admin.users', ['users' => $this->rows])
            ->layout('layouts.app', ['header' => 'Users']);
    }

    protected function rules()
    {
        return [
            'editing.username' => 'required',
            'editing.is_admin' => 'required | boolean',
            'newPassword' => ['sometimes', Password::defaults()],
        ];
    }

    public function delete(User $user)
    {
        $this->deleting = $user;
        $this->showDeleteModal = true;
    }

    public function destroy()
    {
        $this->deleting->delete();
        $this->mount();
        $this->alert('success', $this->deleting->username.' was successfully deleted.');
        $this->reset('showEditModal', 'showDeleteModal');
    }
}
