<?php

namespace App\Http\Livewire\Admin;

use App\Models\Led;
use App\Models\Room;
use Livewire\Component;

class Rooms extends Component
{
    public $showCreateModal = false;
    public $showEditModal = false;
    public $showDeleteModal = false;
    public $selectedLeds = [];
    public Room $editing;
    public Room $deleting;
    public $leds;

    public function mount()
    {
        $this->editing = $this->makeBlankRoom();
        $this->leds = Led::get();
    }

    public function edit(Room $room)
    {
        if ($this->editing->isNot($room)) {
            $this->editing = $room;
        }

        $this->selectedLeds = $this->editing->leds->pluck('id')->map(fn ($led) => (string) $led)->toArray();

        $this->showEditModal = true;
    }

    public function create()
    {
        if ($this->editing->getKey()) {
            $this->editing = $this->makeBlankRoom();
        }

        $this->showCreateModal = true;
    }

    public function delete(Room $room)
    {
        $this->deleting = $room;
        $this->showDeleteModal = true;
    }

    public function destroy()
    {
        $this->deleting->delete();
        $this->editing = $this->makeBlankRoom();
        $this->alert('success', 'Room was successfully deleted.');
        $this->reset('showEditModal', 'showDeleteModal');
    }

    public function store()
    {
        $this->validate();

        $this->editing->save();

        $this->alert('success', 'Rooms was successfully created.');
        $this->reset('showCreateModal');
        $this->editing = $this->makeBlankRoom();
    }

    public function update()
    {
        $this->validate();

        $this->editing->save();

        $leds = $this->editing->leds;

        // delete leds not inside selectedLeds array
        $this->editing->leds()->detach($leds->filter(fn ($led) => !in_array((string) $led->id, $this->selectedLeds))->pluck('id'));

        // create new led if not inside $leds collection but inside selectedLeds array
        $this->editing->leds()->attach(collect($this->selectedLeds)->filter(fn ($led) => !in_array((int) $led, $leds->pluck('id')->toArray()))->toArray());

        $this->alert('success', 'Changes successfully saved.');
        $this->reset('showEditModal', 'selectedLeds');
    }

    public function makeBlankRoom()
    {
        return Room::make();
    }

    public function getRowsProperty()
    {
        return Room::query()->latest()->get();
    }

    public function render()
    {
        return view('livewire.admin.rooms', ['rooms' => $this->rows])
            ->layout('layouts.app', ['header' => 'Rooms']);
    }

    protected function rules()
    {
        return [
            'editing.name' => 'required | max:255',
        ];
    }
}
