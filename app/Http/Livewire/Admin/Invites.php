<?php

namespace App\Http\Livewire\Admin;

use App\Models\Invite;
use Illuminate\Support\Str;
use Livewire\Component;

class Invites extends Component
{
    public Invite $creating;
    public Invite $deleting;
    public $showCreateModal = false;
    public $showDeleteModal = false;
    public $code;

    protected $rules = [
        'creating.name' => 'required',
        'creating.code' => 'required | unique:invites,code',
    ];

    public function mount()
    {
        $this->creating = $this->makeBlankInvite();
    }

    public function makeBlankInvite($override = [])
    {
        return Invite::make($override);
    }

    public function create()
    {
        $this->creating = $this->makeBlankInvite([
            'code' => uniqid(Str::random(13) . '-'),
        ]);

        $this->showCreateModal = true;
    }

    public function delete(Invite $invite)
    {
        $this->deleting = $invite;

        $this->showDeleteModal = true;
    }

    public function destroy()
    {
        $this->deleting->delete();

        $this->alert('success', 'Invite Code successfully deleted');
        $this->reset('showDeleteModal');
    }

    public function save()
    {
        $this->validate();

        $this->creating->save();

        $this->alert('success', 'Invite code successfullt created.');
        $this->reset('showCreateModal');
    }

    public function getRowsProperty()
    {
        return Invite::query()->latest()->get();
    }

    public function render()
    {
        return view('livewire.admin.invites', ['invites' => $this->rows])
            ->layout('layouts.app', ['header' => 'Invites']);
    }
}
