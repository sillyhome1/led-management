<?php

namespace App\Http\Livewire\Admin;

use Illuminate\Support\Str;
use App\Models\AuthToken;
use Livewire\Component;

class AuthTokens extends Component
{
    public $showCreateModal = false;
    public $showDeleteModal = false;
    public AuthToken $deleting;
    public AuthToken $editing;

    public function mount()
    {
        $this->editing = $this->makeNewToken();
    }

    public function create()
    {
        if ($this->editing->getKey()) {
            $this->editing = $this->makeNewToken();
        }

        $this->showCreateModal = true;
    }

    public function delete(AuthToken $room)
    {
        $this->deleting = $room;
        $this->showDeleteModal = true;
    }

    public function destroy()
    {
        $this->deleting->delete();
        $this->editing = $this->makeNewToken();
        $this->alert('success', 'Token was successfully deleted.');
        $this->reset('showDeleteModal');
    }

    public function store()
    {
        $this->validate();

        $this->editing->save();

        $this->alert('success', 'Token was successfully created.');
        $this->reset('showCreateModal');
        $this->editing = $this->makeNewToken();
    }

    public function makeNewToken()
    {
        $token = AuthToken::make();
        $token->auth_token = Str::random(32);
        
        return $token;
    }

    public function getRowsProperty()
    {
        return AuthToken::query()->latest()->get();
    }

    public function render()
    {
        return view('livewire.admin.auth-tokens', ['tokens' => $this->rows])
            ->layout('layouts.app', ['header' => 'Tokens']);
    }

    protected function rules()
    {
        return [
            'editing.auth_token' => 'required | max:255',
        ];
    }
}