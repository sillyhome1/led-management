<?php

namespace App\Http\Livewire\Admin;

use App\Models\Led;
use Livewire\Component;

class Leds extends Component
{
    public $showDeleteModal = false;
    public Led $deleting;

    public function mount()
    {
        $this->deleting = Led::make();
    }

    public function delete(Led $led)
    {
        $this->deleting = $led;
        $this->showDeleteModal = true;
    }

    public function destroy()
    {
        $this->deleting->delete();

        $this->alert('success', 'LED successfully deleted.');
        $this->reset('showDeleteModal');
    }

    public function getRowsProperty()
    {
        return Led::query()->latest()->get();
    }

    public function render()
    {
        return view('livewire.admin.leds', ['leds' => $this->rows])
            ->layout('layouts.app', ['header' => 'LED\'s']);
    }
}
