<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LocalNetwork
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$this->isLocal($request->ip())) {
            abort(403);
        }

        return $next($request);
    }

    private function isLocal($ip)
    {
        # see https://datatracker.ietf.org/doc/html/rfc1918#section-3
        # and https://datatracker.ietf.org/doc/html/rfc5735#section-4
        # for ip-range documentation
        $privateIpRanges = [
            ['10.0.0.0', '10.255.255.255'],
            ['127.0.0.0', '127.255.255.255'],
            ['172.16.0.0', '172.31.255.255'],
            ['192.168.0.0', '192.168.255.255'],
        ];

        foreach ($privateIpRanges as $ipRange) {
            if (
                ip2long($ipRange[0]) <= ip2long($ip) &&
                ip2long($ip) <= ip2long($ipRange[1])
            ) {
                return true;
            }
        }

        return false;
    }
}
