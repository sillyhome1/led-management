<?php

namespace App\Http\Controllers;

use App\Models\Led;
use App\Models\AuthToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LedController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'callback_url' => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $token_success = $this->validateAndUpdateToken($request);
        if (!$token_success) return response("error", 400);

        $led = Led::updateOrCreate(
            [
                'ip' => $request->ip(),
            ],
            [
                'callback_url' => $request->post('callback_url'),
                'ssl' => $request->post('ssl') == 'true',
            ]
        );

        return response($led, 201);
    }

    public function validateAndUpdateToken(Request $request)
    {
        $bearer_header = $request->header('Authorization');
        $bearer_token = explode(' ', $bearer_header)[1];

        $token = AuthToken::where('auth_token', $bearer_token)->firstOrFail();
        if(!$token->led_ip) 
        {
            $token->led_ip = $request->ip();
            $token->save();
            return true;
        } else {
            return $token->led_ip == $request->ip();
        }
    }

    public function destroy(Request $request)
    {
        $led = Led::where('ip', $request->ip())->firstOrFail();

        $led->delete();

        return $led;
    }
}
