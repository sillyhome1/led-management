<?php

namespace App\Providers;

use Livewire\Component;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Component::macro('alert', function($type, $message, $time = 2500) {
            $this->dispatchBrowserEvent('notify', [
                'id' => uniqid(),
                'type' => $type,
                'message' => $message,
                'time' => $time,
            ]);
        });

        Blade::if('admin', function() {
            return auth()->check() && (bool) auth()->user()->is_admin;
        });
    }
}
