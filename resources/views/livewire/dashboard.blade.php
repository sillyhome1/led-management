@once
    @push('styles')
        <link rel="stylesheet" href="{{ asset('css/colr_pickr.min.css') }}">
    @endpush
    @push('scripts')
        <script src="{{ asset('js/colr_pickr.min.js') }}"></script>
    @endpush
@endonce

<div class="bg-gray-900 shadow rounded px-6 py-6 space-y-4">
    <x-input.group borderless for="" label="Select Effect">
        <x-input.select wire:model="effect">
            @foreach (\App\Helpers\EffectHelper::EFFECTS as $effectName)
                <option value="{{ $effectName }}">{{ $effectName }}</option>
            @endforeach
        </x-input.select>
    </x-input.group>

    <hr class="hidden sm:block border-gray-600">

    <div>
        @livewire('effects.' . $effect, key($effect))
    </div>

    <hr class="hidden sm:block pb-6 border-gray-600">

    <div class="grid grid-cols-6 gap-4">
        <div class="col-span-6 md:col-span-2 grid grid-cols-2 gap-4">
            <div class="col-span-1">
                <x-input.group inline for="" label="Send to">
                    <x-input.select wire:model="sendToType">
                        @foreach ($types as $typeKey => $typeName)
                            <option value="{{ $typeKey }}">{{ $typeName }}</option>
                        @endforeach
                    </x-input.select>
                </x-input.group>
            </div>

            <div class="col-span-1">
                <x-input.group inline for="" label="{{ $types[$sendToType] }}">
                    <x-input.select wire:model="sendToEntity">
                        @foreach (${$sendToType . 's'} as $item)
                            <option value="{{ $item->id }}">{{ $sendToType == 'led' ? $item->callback_url : $item->name }}</option>
                        @endforeach
                    </x-input.select>
                </x-input.group>
            </div>
        </div>

        <div class="col-span-6 md:col-span-4 grid grid-cols-3 gap-4 items-end">
            <x-button.secondary wire:click="$emit('show-status')" class="col-span-1 w-full" :disabled="!${$sendToType . 's'}->count()">Status</x-button.secondary>
            <x-button.primary wire:click="prepareSend" class="col-span-2 md:col-span-1 w-full" :disabled="!${$sendToType . 's'}->count()">Update Effect</x-button.primary>
            <x-button.danger wire:click="turnOff" class="col-span-3 md:col-span-1 w-full" :disabled="!${$sendToType . 's'}->count()">Turn Off</x-button.danger>
        </div>
    </div>

    <livewire:status></livewire:status>
</div>
