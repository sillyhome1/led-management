<div>
    <x-table>
        <x-slot name="head">
            <x-table.heading class="text-left">Username</x-table.heading>
            <x-table.heading class="text-left w-8">Type</x-table.heading>
            <x-table.heading class="text-left w-8"></x-table.heading>
        </x-slot>

        <x-slot name="body">
            @forelse ($users as $user)
            <x-table.row>
                <x-table.cell>{{ $user->username }}</x-table.cell>
                <x-table.cell>{{ $user->is_admin ? 'Admin' : 'User' }}</x-table.cell>
                <x-table.cell>
                    <x-button.link wire:click="edit({{ $user->id }})">Edit</x-button.link>
                </x-table.cell>
            </x-table.row>
            @empty
            <x-table.row>
                <x-table.cell colspan="3">
                    <div class="flex justify-center items-center">
                        <span class="font-medium py-8 text-gray-300 text-xl">No users found...</span>
                    </div>
                </x-table.cell>
            </x-table.row>
            @endforelse
        </x-slot>
    </x-table>

    <form wire:submit.prevent="update">
        <x-modal.dialog wire:model="showEditModal">
            <x-slot name="title">Edit User</x-slot>
            <x-slot name="content">
                <x-input.group borderless for="editing.username" label="Username" :error="$errors->first('editing.username')">
                    <x-input.text wire:model="editing.username" placeholder="Username" />
                </x-input.group>

                <x-input.group borderless for="editing.is_admin" label="Type" :error="$errors->first('editing.is_admin')">
                    <x-input.select wire:model="editing.is_admin">
                        <option value="{{ 0 }}">User</option>
                        <option value="{{ 1 }}">Admin</option>
                    </x-input.select>
                </x-input.group>

                <x-input.group borderless for="newPassword" label="New Password" :error="$errors->first('newPassword')">
                    <x-input.password wire:model="newPassword" placeholder="New password" />
                </x-input.group>
            </x-slot>
            <x-slot name="footer">
                <x-button.danger wire:click="delete({{ $editing->id }})">Delete</x-button.danger>
                <x-button.secondary wire:click="$toggle('showEditModal')">Cancel</x-button.secondary>
                <x-button.primary type="submit">Save</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>

    <form wire:submit.prevent="destroy">
        <x-modal.confirmation wire:model="showDeleteModal">
            <x-slot name="title">Delete User</x-slot>
            <x-slot name="content">
                <p>Are you sure you want to delete {{ $editing->username }}?</p>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$toggle('showDeleteModal')">Cancel</x-button.secondary>
                <x-button.danger type="submit">Delete</x-button.danger>
            </x-slot>
        </x-modal>
    </form>
</div>
