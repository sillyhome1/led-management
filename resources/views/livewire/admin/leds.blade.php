<div class="space-y-3">
    <x-table>
        <x-slot name="head">
            <x-table.heading class="text-left">IP</x-table.heading>
            <x-table.heading class="text-left">Callback</x-table.heading>
            <x-table.heading class="text-left">SSL</x-table.heading>
            <x-table.heading class="text-left w-8"></x-table.heading>
        </x-slot>

        <x-slot name="body">
            @forelse ($leds as $led)
            <x-table.row>
                <x-table.cell>{{ $led->ip }}</x-table.cell>
                <x-table.cell>{{ $led->callback_url }}</x-table.cell>
                <x-table.cell>{{ $led->ssl ? 'TRUE' : 'FALSE' }}</x-table.cell>
                <x-table.cell>
                    <x-button.link wire:click="delete({{ $led->id }})" class="text-red-500">Delete</x-button.link>
                </x-table.cell>
            </x-table.row>
            @empty
            <x-table.row>
                <x-table.cell colspan="4">
                    <div class="flex justify-center items-center">
                        <span class="font-medium py-8 text-gray-400 text-xl">No leds found...</span>
                    </div>
                </x-table.cell>
            </x-table.row>
            @endforelse
        </x-slot>
    </x-table>

    <form wire:submit.prevent="destroy">
        <x-modal.confirmation wire:model="showDeleteModal">
            <x-slot name="title">Delete LED</x-slot>
            <x-slot name="content">
                <p>Are you sure you want to delete this LED?</p>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$toggle('showDeleteModal')">Cancel</x-button.secondary>
                <x-button.danger type="submit">Delete</x-button.danger>
            </x-slot>
        </x-modal.confirmation>
    </form>
</div>
