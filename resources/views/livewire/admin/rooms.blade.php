<div class="space-y-3">
    <div class="flex justify-between items-center px-2 sm:px-0">
        <h3 class="text-lg font-medium text-gray-300">Rooms</h3>
        <x-button.primary wire:click="create" class="flex space-x-1 items-center">
            <x-icon.plus size="4" />
            <span>New Room</span>
        </x-button.primary>
    </div>
    <x-table>
        <x-slot name="head">
            <x-table.heading class="text-left">Name</x-table.heading>
            <x-table.heading class="text-left w-8"></x-table.heading>
        </x-slot>

        <x-slot name="body">
            @forelse ($rooms as $room)
            <x-table.row>
                <x-table.cell>{{ $room->name }}</x-table.cell>
                <x-table.cell>
                    <x-button.link wire:click="edit({{ $room->id }})" class="text-gray-300">Edit</x-button.link>
                </x-table.cell>
            </x-table.row>
            @empty
            <x-table.row>
                <x-table.cell colspan="4">
                    <div class="flex justify-center items-center">
                        <span class="font-medium py-8 text-gray-400 text-xl">No rooms found...</span>
                    </div>
                </x-table.cell>
            </x-table.row>
            @endforelse
        </x-slot>
    </x-table>

    <form wire:submit.prevent="store">
        <x-modal.dialog wire:model="showCreateModal">
            <x-slot name="title">Create Room</x-slot>
            <x-slot name="content">
                <x-input.group borderless for="editing.name" label="Name" :error="$errors->first('editing.name')">
                    <x-input.text wire:model.defer="editing.name" placeholder="Name" />
                </x-input.group>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$toggle('showCreateModal')">Cancel</x-button.secondary>
                <x-button.primary type="submit">Create</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>

    <form wire:submit.prevent="update">
        <x-modal.dialog wire:model="showEditModal">
            <x-slot name="title">Edit Room</x-slot>
            <x-slot name="content">
                <x-input.group borderless for="editing.name" label="Name" :error="$errors->first('editing.name')">
                    <x-input.text wire:model.defer="editing.name" placeholder="Name" />
                </x-input.group>

                <x-input.group borderless inline for="" label="Select LED's">
                    <div class="space-y-1">
                        @foreach ($leds as $led)
                        <div class="flex space-x-2 items-center">
                            <input type="checkbox" wire:model.defer="selectedLeds" value="{{ $led->id }}" id="led-{{ $led->id }}">
                            <label for="led-{{ $led->id }}" class="text-gray-300">{{ $led->callback_url }}</label>
                        </div>
                        @endforeach
                    </div>
                </x-input.group>
            </x-slot>
            <x-slot name="footer">
                <x-button.danger wire:click="delete({{ $editing->id }})">Delete</x-button.danger>
                <x-button.secondary wire:click="$toggle('showEditModal')">Cancel</x-button.secondary>
                <x-button.primary type="submit">Save</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>

    <form wire:submit.prevent="destroy">
        <x-modal.confirmation wire:model="showDeleteModal">
            <x-slot name="title">Delete Room</x-slot>
            <x-slot name="content">
                <p>Are you sure you want to delete this Room?</p>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$toggle('showDeleteModal')">Cancel</x-button.secondary>
                <x-button.danger type="submit">Delete</x-button.danger>
            </x-slot>
        </x-modal>
    </form>
</div>
