<div class="space-y-3">
    <div class="flex justify-between items-center px-2 sm:px-0">
        <h3 class="text-lg font-medium text-gray-300">Invite Codes</h3>
        <x-button.primary wire:click="create" class="flex space-x-1 items-center">
            <x-icon.plus size="4" />
            <span>Create Invite</span>
        </x-button.primary>
    </div>
    <x-table>
        <x-slot name="head">
            <x-table.heading class="text-left">Name</x-table.heading>
            <x-table.heading class="text-left">Code</x-table.heading>
            <x-table.heading class="text-left">Date</x-table.heading>
            <x-table.heading class="text-left w-8"></x-table.heading>
        </x-slot>

        <x-slot name="body">
            @forelse ($invites as $invite)
            <x-table.row>
                <x-table.cell>{{ $invite->name }}</x-table.cell>
                <x-table.cell>
                    <a href="{{ route('register', $invite->code) }}" class="text-indigo-500">{{ route('register', $invite->code) }}</a>
                </x-table.cell>
                <x-table.cell>{{ $invite->created_at->format('d M Y') }}</x-table.cell>
                <x-table.cell>
                    <x-button.link wire:click="delete({{ $invite->id }})" class="text-red-500">Delete</x-button.link>
                </x-table.cell>
            </x-table.row>
            @empty
            <x-table.row>
                <x-table.cell colspan="4">
                    <div class="flex justify-center items-center">
                        <span class="font-medium py-8 text-gray-400 text-xl">No invites found...</span>
                    </div>
                </x-table.cell>
            </x-table.row>
            @endforelse
        </x-slot>
    </x-table>

    <form wire:submit.prevent="save">
        <x-modal.dialog wire:model="showCreateModal">
            <x-slot name="title">Create Invite</x-slot>
            <x-slot name="content">
                <x-input.group borderless for="creating.name" label="Name" :error="$errors->first('creating.name')">
                    <x-input.text wire:model="creating.name" placeholder="Name" />
                </x-input.group>

                <x-input.group borderless for="creating.code" label="Code" :error="$errors->first('creating.code')">
                    <x-input.text wire:model="creating.code" placeholder="Code" disabled />
                </x-input.group>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$toggle('showCreateModal')">Cancel</x-button.secondary>
                <x-button.primary type="submit">Create</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>

    <form wire:submit.prevent="destroy">
        <x-modal.confirmation wire:model="showDeleteModal">
            <x-slot name="title">Delete Invite Code</x-slot>
            <x-slot name="content">
                <p>Are you sure you want to delete this invite code?</p>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$toggle('showDeleteModal')">Cancel</x-button.secondary>
                <x-button.danger type="submit">Delete</x-button.danger>
            </x-slot>
        </x-modal.confirmation>
    </form>
</div>
