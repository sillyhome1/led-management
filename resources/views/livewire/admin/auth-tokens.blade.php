<div class="space-y-3">
    <div class="flex justify-between items-center px-2 sm:px-0">
        <h3 class="text-lg font-medium text-gray-300">Tokens</h3>
        <x-button.primary wire:click="create" class="flex space-x-1 items-center">
            <x-icon.plus size="4" />
            <span>New Token</span>
        </x-button.primary>
    </div>
    <x-table>
        <x-slot name="head">
            <x-table.heading class="text-left">Token</x-table.heading>
            <x-table.heading class="text-left">IP</x-table.heading>
            <x-table.heading class="text-left w-8"></x-table.heading>
        </x-slot>

        <x-slot name="body">
            @forelse ($tokens as $token)
            <x-table.row>
                <x-table.cell>{{ $token->auth_token }}</x-table.cell>
                <x-table.cell class="flex items-center space-x-3 {{ $token->led_ip ? '' : 'text-gray-400' }}">
                    @if($token->led_ip)
                    {{-- check-circle icon from heroicons --}}
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                    <span>{{ $token->led_ip }}</span>
                    @else
                    {{-- x-circle icon from heroicons --}}
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                    <span>no device has registered</span>
                    @endif
                </x-table.cell>
                <x-table.cell>
                    <x-button.link wire:click="delete({{ $token->id }})" class="text-red-500">Delete</x-button.link>
                </x-table.cell>
            </x-table.row>
            @empty
            <x-table.row>
                <x-table.cell colspan="4">
                    <div class="flex justify-center items-center">
                        <span class="font-medium py-8 text-gray-400 text-xl">No tokens found...</span>
                    </div>
                </x-table.cell>
            </x-table.row>
            @endforelse
        </x-slot>
    </x-table>

    <form wire:submit.prevent="store">
        <x-modal.dialog wire:model="showCreateModal">
            <x-slot name="title">Create Token</x-slot>
            <x-slot name="content">
                <x-input.group borderless for="editing.auth_token" label="Token" :error="$errors->first('editing.auth_token')">
                    <x-input.text wire:model.defer="editing.auth_token" placeholder="Token" disabled/>
                </x-input.group>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$toggle('showCreateModal')">Cancel</x-button.secondary>
                <x-button.primary type="submit">Create</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>

    <form wire:submit.prevent="destroy">
        <x-modal.confirmation wire:model="showDeleteModal">
            <x-slot name="title">Delete Token</x-slot>
            <x-slot name="content">
                <p>Are you sure you want to delete this Token?</p>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$toggle('showDeleteModal')">Cancel</x-button.secondary>
                <x-button.danger type="submit">Delete</x-button.danger>
            </x-slot>
        </x-modal>
    </form>
</div>