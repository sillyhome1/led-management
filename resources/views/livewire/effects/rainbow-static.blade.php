<div>
    <x-input.group borderless for="" label="Duration">
        <x-input.text wire:model.defer="duration" placeholder="Duration" />
    </x-input.group>

    <x-input.group borderless for="" label="Reverse">
        <x-input.select wire:model.defer="reverse">
            <option value="0">No</option>
            <option value="true">Yes</option>
        </x-input.select>
    </x-input.group>
</div>
