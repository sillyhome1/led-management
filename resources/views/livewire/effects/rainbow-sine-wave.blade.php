<div x-data="{ brightness: @entangle('brightness').defer }">
    <x-input.group borderless for="" label="Duration">
        <x-input.text wire:model.defer="duration" placeholder="Duration" />
    </x-input.group>

    <x-input.group borderless for="" label="Reverse">
        <x-input.select wire:model.defer="reverse">
            <option value="0">No</option>
            <option value="true">Yes</option>
        </x-input.select>
    </x-input.group>

    <x-input.group borderless for="" label="Brightness">
        <div class="flex space-x-2">
            <p class="text-gray-400 w-10" x-text="brightness"></p>
            <input type="range" x-model="brightness" min="0" max="1" step="0.01" class="w-full">
        </div>
    </x-input.group>
</div>
