<div>
    <x-input.group borderless for="" label="Duration">
        <x-input.text wire:model.defer="duration" placeholder="Duration" />
    </x-input.group>


    <x-input.group borderless for="" label="Colors">
        <div class="grid grid-cols-5 gap-4 text-gray-300">
            @foreach ($colors as $color)
                <div class="col-span-1">{{ $loop->index + 1 }}</div>
                <div class="col-span-3 w-full">
                    <input type="color" wire:model="colors.{{ $loop->index }}">
                </div>
                <div class="col-span-1 text-right">
                    <x-icon.trash size="6" wire:click="removeColor({{ $loop->index }})" />
                </div>
            @endforeach

            <div class="col-span-5">
                <x-button.primary wire:click="addColor">Add Color</x-button.primary>
            </div>
        </div>

    </x-input.group>
</div>
