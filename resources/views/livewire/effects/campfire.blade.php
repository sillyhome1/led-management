<div x-data="{ hue: @entangle('hue').defer, turbulence: @entangle('turbulence').defer }">
    <x-input.group borderless for="" label="Hue">
        <div class="flex space-x-2">
            <p class="text-gray-400 w-10" x-text="hue"></p>
            <input type="range" x-model="hue" min="0" max="360" class="w-full">
        </div>
    </x-input.group>

    <x-input.group borderless for="" label="Turbulence">
        <div class="flex space-x-2">
            <p class="text-gray-400 w-10" x-text="turbulence"></p>
            <input type="range" x-model="turbulence" min="0" max="2" step="0.01" class="w-full">
        </div>
    </x-input.group>
</div>
