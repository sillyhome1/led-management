<div x-data="{ hue: @entangle('hue').defer, speed: @entangle('speed').defer }">
    <x-input.group borderless for="" label="Hue">
        <div class="flex space-x-2">
            <p class="text-gray-400 w-10" x-text="hue"></p>
            <input type="range" x-model="hue" min="0" max="360" class="w-full">
        </div>
    </x-input.group>

    <x-input.group borderless for="" label="Speed">
        <div class="flex space-x-2">
            <p class="text-gray-400 w-10" x-text="speed"></p>
            <input type="range" x-model="speed" min="0" max="10" step="0.01" class="w-full">
        </div>
    </x-input.group>

    <x-input.group borderless for="" label="Reverse">
        <x-input.select wire:model.defer="reverse">
            <option value="0">No</option>
            <option value="true">Yes</option>
        </x-input.select>
    </x-input.group>
</div>
