<div
    x-data="{ color: @entangle('color'), button: null, picker: null, newColor: '#ffffff' }"
    x-init="
        button = $refs.colorpicker
        picker = new ColorPicker(button, color)

        let timeout;

        button.addEventListener('colorChange', function (event) {
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                color = event.detail.color.hexa;
            }, 100);
        });
    "
>
    <div wire:ignore>
        <x-input.group borderless for="" label="Select Color">
            <x-button x-ref="colorpicker" class="w-full h-32"></x-button>
        </x-input.group>
    </div>
</div>
