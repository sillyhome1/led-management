<x-modal.dialog wire:model="show">
    <x-slot name="title">LED Status</x-slot>
    <x-slot name="content">
        <div class="space-y-3">
            @foreach($leds->whereNotNull('status') as $led)
                <details class="cursor-pointer">
                    <summary class="text-gray-300 px-2 py-1 bg-gray-700 border border-gray-600 font-mono">{{ $led->callback_url }}</summary>

                    <div class="bg-gray-700 border border-gray-600 p-4 text-gray-300 overflow-x-auto">
                        <pre>{{ $led?->status?->json }}</pre>
                    </div>
                </details>
            @endforeach
        </div>
    </x-slot>
    <x-slot name="footer">
        <x-button.secondary wire:click="$toggle('show')">Close</x-button.secondary>
    </x-slot>
</x-modal.dialog>
