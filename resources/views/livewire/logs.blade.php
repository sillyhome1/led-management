<div class="space-y-3">
    <x-table>
        <x-slot name="head">
            <x-table.heading class="text-left">User</x-table.heading>
            <x-table.heading class="text-left">Effect</x-table.heading>
            <x-table.heading class="text-left whitespace-nowrap">Send To Type</x-table.heading>
            <x-table.heading class="text-left whitespace-nowrap">Send To Entity</x-table.heading>
            <x-table.heading class="text-left">Properties</x-table.heading>
            <x-table.heading class="text-left">Date</x-table.heading>
        </x-slot>

        <x-slot name="body">
            @forelse ($logs as $log)
            <x-table.row>
                <x-table.cell>{{ $log->user->username }}</x-table.cell>
                <x-table.cell class="whitespace-nowrap">{{ $log->effect }}</x-table.cell>
                <x-table.cell>{{ $log->send_to_type }}</x-table.cell>
                <x-table.cell class="whitespace-nowrap">
                    @if ($log->send_to_type === 'room')
                        {{ $rooms->find($log->send_to_entity)->name ?? 'deleted' }}
                    @endif
                    @if ($log->send_to_type === 'led')
                        {{ $leds->find($log->send_to_entity)->callback_url ?? 'deleted' }}
                    @endif
                </x-table.cell>
                <x-table.cell class="whitespace-nowrap">{{ $log->properties }}</x-table.cell>
                <x-table.cell class="whitespace-nowrap">{{ $log->created_at->format('d.m.Y H:i:s') }}</x-table.cell>
            </x-table.row>
            @empty
            <x-table.row>
                <x-table.cell colspan="6">
                    <div class="flex justify-center items-center">
                        <span class="font-medium py-8 text-gray-400 text-xl">No leds found...</span>
                    </div>
                </x-table.cell>
            </x-table.row>
            @endforelse
        </x-slot>
    </x-table>

    <div>
        {{ $logs->links() }}
    </div>
</div>
