<div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
    <table class="min-w-full divide-y divide-gray-600">
        <thead>
            <tr>
                {{ $head }}
            </tr>
        </thead>

        <tbody class="bg-gray-800 text-gray-300 divide-y divide-gray-600">
            {{ $body }}
        </tbody>
    </table>
</div>
