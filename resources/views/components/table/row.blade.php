<tr {{ $attributes->merge(['class' => 'bg-gray-700']) }}>
    {{ $slot }}
</tr>
