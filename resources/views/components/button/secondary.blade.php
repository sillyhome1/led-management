<x-button {{ $attributes->class(['border-gray-600 text-gray-300 active:bg-gray-300 active:text-gray-800 hover:text-gray-200'])->merge() }}>{{ $slot }}</x-button>
