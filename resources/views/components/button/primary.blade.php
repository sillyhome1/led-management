<x-button
    {{ $attributes->class(['text-white bg-indigo-600 hover:bg-indigo-500 active:bg-indigo-700 border-indigo-600'])->merge() }}
>
    {{ $slot }}
</x-button>
