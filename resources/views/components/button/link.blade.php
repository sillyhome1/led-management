<button
    {{ $attributes->class([
        'text-gray-300 text-sm leading-5 font-medium focus:outline-none focus:text-gray-200 focus:underline transition duration-150 ease-in-out',
        'opacity-75 cursor-not-allowed' => $attributes->get('disabled')
    ])->merge(['type' => 'button']) }}
>
    {{ $slot }}
</button>
