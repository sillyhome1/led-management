@props([
    'leadingAddOn' => false,
])

<div class="flex rounded-md shadow-sm">
    @if ($leadingAddOn)
        <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-600 bg-gray-50 text-gray-500 sm:text-sm">
            {{ $leadingAddOn }}
        </span>
    @endif

    <input {{ $attributes->class([
        'flex-1 form-input border-gray-600 bg-gray-800 text-gray-300 placeholder-gray-400 block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5',
        'rounded-none rounded-r-md' => $leadingAddOn,
        'rounded-md' => !$leadingAddOn
    ])->merge(['type' => 'text']) }}/>
</div>
