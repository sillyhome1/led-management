<a {{ $attributes->merge(['class' => 'block px-4 py-2 text-sm leading-5 text-gray-300 hover:bg-gray-900 focus:outline-none focus:bg-gray-900 transition duration-150 ease-in-out']) }}>{{ $slot }}</a>
