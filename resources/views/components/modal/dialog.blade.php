@props(['id' => null, 'maxWidth' => null])

<x-modal :id="$id" :maxWidth="$maxWidth" {{ $attributes }}>
    <div class="text-lg text-gray-300 px-6 py-4">
        {{ $title }}
    </div>

    <div class="border-t px-6 py-4 border-gray-700 mb-4">
        {{ $content }}
    </div>

    <div class="text-right border-t px-6 py-4 border-gray-700">
        {{ $footer }}
    </div>
</x-modal>
