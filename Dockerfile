FROM docker.io/php:8.2

# we need to update our package lists to be able to install anything
# the php image has all extensions compiled-in
# we just need apache, node and npm
RUN apt update \
    && apt install nodejs npm unzip -y \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# copy application files
ADD ./ /var/www/html/led-management
WORKDIR /var/www/html/led-management
#RUN chmod 700 /var/www/html/led-management

# write start-script
RUN printf '\
    /usr/local/bin/php /var/www/html/led-management/artisan migrate --force \n\
    /usr/local/bin/php /var/www/html/led-management/artisan serve --host=0.0.0.0 --port=8000 \n\
    ' > /var/www/html/led-management/start-laravel

# install node dependencies
RUN npm update \
    && npm run prod

# install composer and our composer dependencies
RUN curl -sLS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer \
    && /usr/bin/composer update

EXPOSE 8000

CMD ["/bin/bash", "/var/www/html/led-management/start-laravel"]
# for tesing
# podman build . -t pedda/led-management:latest
# or
# podman build . -t pedda/led-management:latest | tail -n 1 | xargs podman run
# or
# podman run pedda/led-management:latest
# or
# podman-compose up --build --force-recreate --no-deps
